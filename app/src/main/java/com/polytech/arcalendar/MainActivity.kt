package com.polytech.arcalendar

import android.app.ActivityManager
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.ar.core.Config
import com.google.ar.core.Session
import com.polytech.ARCalendar.R
import kotlinx.android.synthetic.main.fragment_text_list_dialog.*
import kotlinx.android.synthetic.main.welcome_screen.*
import com.google.android.material.bottomsheet.BottomSheetBehavior as BottomSheetBehavior


class MainActivity : AppCompatActivity() {
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>
    private lateinit var prefs: SharedPreferences
    private val openGlVersion by lazy {
        (getSystemService(ACTIVITY_SERVICE) as ActivityManager)
            .deviceConfigurationInfo
            .glEsVersion
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        prefs = getSharedPreferences("settings", Context.MODE_PRIVATE)
        if (!prefs.contains("timesOpened")) {
            setContentView(R.layout.welcome_screen)
            step1.movementMethod = LinkMovementMethod.getInstance()
            button_to_skip.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    runMainApp()
                }
            })
        } else {
            runMainApp()
        }
    }

    private fun runMainApp() {
        setContentView(R.layout.activity_main)
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        bottomSheetBehavior.halfExpandedRatio = 0.35F
        bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                val upperState = 0.50
                val lowerState = 0.25
                if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_SETTLING) {
                    if (slideOffset >= upperState) {
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                    }
                    if (slideOffset > lowerState && slideOffset < upperState) {
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HALF_EXPANDED
                    }
                }
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
            }
        })

        if (openGlVersion.toDouble() >= MIN_OPEN_GL_VERSION) {
            supportFragmentManager.inTransaction {
                replace(
                    R.id.fragmentContainer,
                    ArVideoFragment()
                )
            }
        } else {
            AlertDialog.Builder(this)
                .setTitle("Device is not supported")
                .setMessage("OpenGL ES 3.0 or higher is required. The device is running OpenGL ES $openGlVersion.")
                .setPositiveButton(android.R.string.ok) { _, _ -> finish() }
                .show()
        }
    }

    private inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
        beginTransaction().func().commit()
    }

    override fun onStop() {
        super.onStop()
        val editor = prefs.edit()
        editor.putInt("timesOpened", 1).apply()
    }

    companion object {
        private const val MIN_OPEN_GL_VERSION = 3.0
    }
}